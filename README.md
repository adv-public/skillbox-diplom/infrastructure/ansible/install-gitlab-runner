GitLab Runner 
=============

This role will install the [official GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner)
(fork from haroldb) with updates. Needed something simple and working, this did the trick for me. Open for changes though.

Requirements
------------

This role requires Ansible 2.7 or higher.

Role Variables
--------------

- `gitlab_runner_package_name` - **Since Gitlab 10.x** The package name of `gitlab-ci-multi-runner` has been renamed to `gitlab-runner`. In order to install a version < 10.x you will need to define this variable `gitlab_runner_package_name: gitlab-ci-multi-runner`.
- `gitlab_runner_wanted_version` or `gitlab_runner_package_version` - To install a specific version of the gitlab runner (by default it installs the latest).
On Mac OSX and Windows, use e.g. `gitlab_runner_wanted_version: 12.4.1`.
On Linux, use `gitlab_runner_package_version` instead.
- `gitlab_runner_concurrent` - The maximum number of global jobs to run concurrently. Defaults to the number of processor cores.
- `gitlab_runner_registration_token` - The GitLab registration token. If this is specified, a runner will be registered to a GitLab server.
- `gitlab_runner_coordinator_url` - The GitLab coordinator URL. Defaults to `https://gitlab.com`.
- `gitlab_runner_sentry_dsn` - Enable tracking of all system level errors to Sentry
- `gitlab_runner_listen_address` - Enable `/metrics` endpoint for Prometheus scraping.
- `gitlab_runner_runners` - A list of gitlab runners to register & configure. Defaults to a single shell executor.
- `gitlab_runner_skip_package_repo_install`- Skip the APT or YUM repository installation (by default, false). You should provide a repository containing the needed packages before running this role.

See the [`defaults/main.yml`](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/install-gitlab-runner/-/blob/main/roles/gitlab-runner/defaults/main.yml) file listing all possible options which you can be passed to a runner registration command.

### Read Sources
For details follow these links:

- [gitlab-docs/runner: advanced configuration: runners.machine section](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnersmachine-section)

See the [config for more options](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/install-gitlab-runner/-/blob/main/roles/gitlab-runner/tasks/register-runner.yml)

Example Playbook
----------------

```yaml
- hosts: all
  become: true
  vars_files:
    - vars/main.yml
  roles:
    - { role: install-gitlab-runner }
```

Inside `vars/main.yml`

```yaml
gitlab_runner_coordinator_url: https://gitlab.com
gitlab_runner_registration_token: '12341234'
gitlab_runner_runners:
  - name: 'Example Docker GitLab Runner'
    # url is an optional override to the global gitlab_runner_coordinator_url
    url: 'https://my-own-gitlab.mydomain.com'
    executor: shell
    tags:
      - linux
      - shell
```

Running Playbook
----------------

```shell
ansible-playbook playbooks/pl_register_runner.yml --extra-vars gitlab_runner_registration_token=<'The GitLab registration token'>
``` 
